﻿using cbspline.Math;
using OxyPlot;
using OxyPlot.Series;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace cbspline
{
    public partial class MainForm : Form
    {
        private const int Order = 3;

        private readonly OxyPlotView plotView;

        private readonly LineSeries spline;
        private readonly LineSeries controlPolygon;
        private readonly LineSeries controlPolygonPoints;
        private readonly LineSeries tangentPoly;
        private readonly LineSeries tangentPolyIntersects;

        public bool ShowControlPolygon { get; set; } = true;
        public bool ShowTangenPolygon { get; set; }

        public MainForm()
        {
            InitializeComponent();

            plotView = this.oxyPlotView1;

            // define all series
            controlPolygonPoints = new LineSeries()
            {
                MarkerType = MarkerType.Circle,
                MarkerSize = 5,
                MarkerFill = OxyColor.FromRgb(33, 33, 33),
                // MarkerStrokeThickness = 20,
                LineStyle = LineStyle.None,
            };

            tangentPoly = new LineSeries()
            {
                Color = OxyColors.Black,
                MarkerType = MarkerType.Circle,
                MarkerSize = 4,
                MarkerFill = OxyColors.DarkRed,
                MarkerStrokeThickness = 20,
            };

            tangentPolyIntersects = new LineSeries()
            {
                Color = OxyColors.Black,
                MarkerType = MarkerType.Circle,
                MarkerSize = 6,
                MarkerFill = OxyColors.Black,
                MarkerStrokeThickness = 20,
                 LineStyle = LineStyle.None
            };

            spline = new LineSeries
            {
                Color = OxyColor.FromRgb(211, 47, 47),
                StrokeThickness = 4
            };

            controlPolygon = new LineSeries
            {
                Color = OxyColor.FromRgb(96, 125, 139)
            };

            // add all series
            plotView.CreateMovablePlotModel(controlPolygonPoints, plotView.AxesPlotModel);
            plotView.Model.Series.Add(spline);
            plotView.Model.Series.Add(tangentPoly);
            plotView.Model.Series.Add(tangentPolyIntersects);
            plotView.Model.Series.Add(controlPolygon);


            plotView.RefreshPlot += PlotViewOnRefreshPlot;
        }

        private void PlotViewOnRefreshPlot(object sender, EventArgs eventArgs)
        {
            if (controlPolygonPoints.Points.Count >= 2)
            {
                spline.Points.Clear();
                controlPolygon.Points.Clear();

                foreach (var p in controlPolygonPoints.Points)
                {
                    controlPolygon.Points.Add(new DataPoint(p.X, p.Y));
                }

                var ft = Bspline.Create(controlPolygonPoints.Points.Select(p => p.ToPointF()).ToList(), Order);

                var a = 0;
                var b = controlPolygonPoints.Points.Count - Order + 1;
                var h = 0.05;
                var r = (int)System.Math.Round((b - a) / h);
                for (var i = 0; i <= r; i++)
                {
                    var t = a + h * i;
                    var d = ft(t);
                    spline.Points.Add(new DataPoint(d[0], d[1]));
                }
            }

            if (ShowTangenPolygon)
            {
                var targ = trcTangentPolyArg.Value / 100.0;
                DrawTanegentBuildingLines(targ);
            }

            plotView.Redraw();
        }

        private void DrawTanegentBuildingLines(double t)
        {
            if (controlPolygonPoints.Points.Count < 3) return;

            var _dots = controlPolygonPoints.Points;

            var dots = new List<DataPoint>();
            dots.Add(_dots.First());
            for (var i = 1; i < _dots.Count - 2; i++)
            {
                var p1 = _dots[i];
                var p2 = _dots[i + 1];

                var middle = Utils.ParametricPoint(0.5, p2, p1);
                dots.Add(p1);
                dots.Add(middle);
            }
            dots.Add(_dots[_dots.Count - 2]);
            dots.Add(_dots.Last());

            var count = dots.Count;

            tangentPoly.Points.Clear();
            tangentPolyIntersects.Points.Clear();

            for (var i = 0; i < count - 1; i++)
            {
                tangentPoly.Points.Add(Utils.ParametricPoint(t, dots[i + 1], dots[i]));
            }

            var pts = tangentPoly.Points;
            for (var i = 0; i < pts.Count - 1; i += 2)
            {
                tangentPolyIntersects.Points.Add(Utils.ParametricPoint(t, pts[i + 1], pts[i]));
            }
        }

        private async Task DrawTanegentBuildingLinesAsync()
        {
            var sleep = 500 / trackBar2.Value;

            await Task.Run(() =>
            {
                var t = 0.0;
                while (t <= 1.0)
                {
                    t += 0.05f;

                    DrawTanegentBuildingLines(t);

                    tangentPoly.IsVisible = rbTangentAll.Checked || rbTangentsOnly.Checked;
                    tangentPolyIntersects.IsVisible = rbTangentAll.Checked || rbTangentintersectsOnly.Checked;

                    plotView.Redraw();
               

                    label1.Invoke((MethodInvoker)(() =>
                    {
                        label1.Text = $"t = {t:N2}";
                    }));

                    Thread.Sleep(sleep);
                }

            });

              SetTangentVisibility();

        }

        private void SetTangentVisibility()
        {
            if (rbTangentAll.Checked)
            {
                tangentPoly.IsVisible = ShowTangenPolygon;
                tangentPolyIntersects.IsVisible = ShowTangenPolygon;
            }
            else if (rbTangentsOnly.Checked)
            {
                tangentPoly.IsVisible = ShowTangenPolygon;
                tangentPolyIntersects.IsVisible = false;
            }
            else
            {
                tangentPoly.IsVisible = false;
                tangentPolyIntersects.IsVisible = ShowTangenPolygon;
            }
        }

        private void trcTangentPolyArg_Scroll(object sender, EventArgs e)
        {
            if (ShowTangenPolygon)
            {
                SetTangentVisibility();

                var t = trcTangentPolyArg.Value / 100.0;
                DrawTanegentBuildingLines(t);

                plotView.Redraw();

                label1.Text = $"t = {trcTangentPolyArg.Value / 100.0}";
            }
        }

        private void chkShowControlPoly_CheckedChanged(object sender, EventArgs e)
        {
            ShowControlPolygon = chkShowControlPoly.Checked;

            controlPolygon.IsVisible = ShowControlPolygon;

            plotView.Redraw();
        }

        private void chkShowTangentPoly_CheckedChanged(object sender, EventArgs e)
        {
            ShowTangenPolygon = chkShowTangentPoly.Checked;

            trcTangentPolyArg.Enabled = ShowTangenPolygon;

            tangentPoly.IsVisible = ShowTangenPolygon;
            tangentPolyIntersects.IsVisible = ShowTangenPolygon;

            SetTangentVisibility();

            var targ = trcTangentPolyArg.Value / 100.0;
            DrawTanegentBuildingLines(targ);

            plotView.Redraw();
        }

        private async void btnPlayTangentDraw_Click(object sender, EventArgs e)
        {
            if (controlPolygon.Points.Count < 2) return;

            tangentPoly.IsVisible = true;
            tangentPolyIntersects.IsVisible = true;

            button1.Enabled = false;

            await DrawTanegentBuildingLinesAsync();

            button1.Enabled = true;

            tangentPoly.IsVisible = ShowTangenPolygon;
            tangentPolyIntersects.IsVisible = ShowTangenPolygon;

            plotView.Redraw();
        }

        private void rbTangentView_CheckedChanged(object sender, EventArgs e)
        {
            SetTangentVisibility();

            plotView.Redraw();
        }

    }
}

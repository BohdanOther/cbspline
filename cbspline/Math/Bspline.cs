﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace cbspline.Math
{
    public class Bspline
    {
        public static Func<double, double[]> Create(List<PointF> points, int order)
        {
            Func<double, double[]> ft = t =>
            {
                var len = points.Count;
                var n = len - 1;
                var k = order;

                if (n <= 0)
                {
                    return new[] { double.NaN, double.NaN };
                }

                if (n + 2 <= k)
                {
                    k = n - 1;
                }

                if (t <= 0)
                {
                    return new[] { (double)points[0].X, (double)points[0].Y };
                }

                if (t >= n - k + 2)
                {
                    return new[] { (double)points[n].X, (double)points[n].Y };
                }

                var s = (int)System.Math.Floor(t) + k - 1;

                var knots = _knotVector(n, k);

                var N = _evalBasisFuncs(t, knots, n, k, s);

                var y = 0.0;
                var x = 0.0;
                for (var j = s - k + 1; j <= s; j++)
                {
                    if (j < len && j >= 0)
                    {
                        y += points[j].Y * N[j];
                        x += points[j].X * N[j];
                    }
                }

                return new[] { x, y };
            };

            return ft;
        }

        public static double[] _knotVector(int n, int k)
        {
            var kn = new double[n + k + 1];

            for (var j = 0; j < n + k + 1; j++)
            {
                if (j < k)
                {
                    kn[j] = 0.0;
                }
                else if (j <= n)
                {
                    kn[j] = j - k + 1;
                }
                else
                {
                    kn[j] = n - k + 2;
                }
            }

            return kn;
        }

        public static double[] _evalBasisFuncs(double t, double[] kn, int n, int k, int s)
        {
            var N = new double[s + 1];

            if (kn[s] <= t && t < kn[s + 1])
            {
                N[s] = 1;
            }
            else
            {
                N[s] = 0;
            }

            for (var i = 2; i <= k; i++)
            {
                for (var j = s - i + 1; j <= s; j++)
                {
                    double a;
                    if (j <= s - i + 1 || j < 0)
                    {
                        a = 0.0;
                    }
                    else
                    {
                        a = N[j];
                    }

                    var b = j >= s ? 0.0 : N[j + 1];

                    var den = kn[j + i - 1] - kn[j];

                    if (den.Equals(0.0))
                    {
                        N[j] = 0;
                    }
                    else
                    {
                        N[j] = (t - kn[j]) / den * a;
                    }

                    den = kn[j + i] - kn[j + 1];

                    if (!den.Equals(0.0))
                    {
                        N[j] += (kn[j + i] - t) / den * b;
                    }
                }
            }
            return N;
        }
    }
}

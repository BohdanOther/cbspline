﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OxyPlot;

namespace cbspline.Math
{
    public static class Utils
    {
        public static PointF ToPointF(this DataPoint p)
        {
            return new PointF((float)p.X, (float)p.Y);
        }

        public static DataPoint ToDataPoint(this PointF p)
        {
            return new DataPoint(p.X, p.Y);
        }

        public static DataPoint ParametricPoint(double t, DataPoint p1, DataPoint p2)
        {
            var x = t * p1.X + (1.0 - t) * p2.X;
            var y = t * p1.Y + (1.0 - t) * p2.Y;

            return new DataPoint(x, y);
        }
    }
}

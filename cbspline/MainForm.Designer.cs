﻿namespace cbspline
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkShowControlPoly = new System.Windows.Forms.CheckBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.rbTangentintersectsOnly = new System.Windows.Forms.RadioButton();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.rbTangentsOnly = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.rbTangentAll = new System.Windows.Forms.RadioButton();
            this.chkShowTangentPoly = new System.Windows.Forms.CheckBox();
            this.trcTangentPolyArg = new System.Windows.Forms.TrackBar();
            this.oxyPlotView1 = new cbspline.OxyPlotView();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trcTangentPolyArg)).BeginInit();
            this.SuspendLayout();
            // 
            // chkShowControlPoly
            // 
            this.chkShowControlPoly.AutoSize = true;
            this.chkShowControlPoly.Checked = true;
            this.chkShowControlPoly.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkShowControlPoly.Location = new System.Drawing.Point(15, 25);
            this.chkShowControlPoly.Name = "chkShowControlPoly";
            this.chkShowControlPoly.Size = new System.Drawing.Size(126, 17);
            this.chkShowControlPoly.TabIndex = 1;
            this.chkShowControlPoly.Text = "show control polygon";
            this.chkShowControlPoly.UseVisualStyleBackColor = true;
            this.chkShowControlPoly.CheckedChanged += new System.EventHandler(this.chkShowControlPoly_CheckedChanged);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 0);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            this.splitContainer1.Panel1.Controls.Add(this.chkShowControlPoly);
            this.splitContainer1.Panel1.Controls.Add(this.chkShowTangentPoly);
            this.splitContainer1.Panel1.Padding = new System.Windows.Forms.Padding(3);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.oxyPlotView1);
            this.splitContainer1.Size = new System.Drawing.Size(721, 497);
            this.splitContainer1.SplitterDistance = 181;
            this.splitContainer1.TabIndex = 2;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.rbTangentintersectsOnly);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.trackBar2);
            this.groupBox1.Controls.Add(this.rbTangentsOnly);
            this.groupBox1.Controls.Add(this.trcTangentPolyArg);
            this.groupBox1.Controls.Add(this.rbTangentAll);
            this.groupBox1.Location = new System.Drawing.Point(3, 108);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(175, 273);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Tangent polygon";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 56);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(37, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "t = 0.0";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 213);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(38, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Speed";
            // 
            // rbTangentintersectsOnly
            // 
            this.rbTangentintersectsOnly.AutoSize = true;
            this.rbTangentintersectsOnly.Location = new System.Drawing.Point(15, 138);
            this.rbTangentintersectsOnly.Name = "rbTangentintersectsOnly";
            this.rbTangentintersectsOnly.Size = new System.Drawing.Size(93, 17);
            this.rbTangentintersectsOnly.TabIndex = 2;
            this.rbTangentintersectsOnly.Text = "Intersects only";
            this.rbTangentintersectsOnly.UseVisualStyleBackColor = true;
            this.rbTangentintersectsOnly.CheckedChanged += new System.EventHandler(this.rbTangentView_CheckedChanged);
            // 
            // trackBar2
            // 
            this.trackBar2.AutoSize = false;
            this.trackBar2.Location = new System.Drawing.Point(53, 211);
            this.trackBar2.Minimum = 1;
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(107, 23);
            this.trackBar2.TabIndex = 7;
            this.trackBar2.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trackBar2.Value = 5;
            // 
            // rbTangentsOnly
            // 
            this.rbTangentsOnly.AutoSize = true;
            this.rbTangentsOnly.Location = new System.Drawing.Point(15, 115);
            this.rbTangentsOnly.Name = "rbTangentsOnly";
            this.rbTangentsOnly.Size = new System.Drawing.Size(92, 17);
            this.rbTangentsOnly.TabIndex = 1;
            this.rbTangentsOnly.Text = "Tangents only";
            this.rbTangentsOnly.UseVisualStyleBackColor = true;
            this.rbTangentsOnly.CheckedChanged += new System.EventHandler(this.rbTangentView_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(12, 240);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(145, 23);
            this.button1.TabIndex = 6;
            this.button1.Text = "play tangent loop";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.btnPlayTangentDraw_Click);
            // 
            // rbTangentAll
            // 
            this.rbTangentAll.AutoSize = true;
            this.rbTangentAll.Checked = true;
            this.rbTangentAll.Location = new System.Drawing.Point(15, 92);
            this.rbTangentAll.Name = "rbTangentAll";
            this.rbTangentAll.Size = new System.Drawing.Size(36, 17);
            this.rbTangentAll.TabIndex = 0;
            this.rbTangentAll.TabStop = true;
            this.rbTangentAll.Text = "All";
            this.rbTangentAll.UseVisualStyleBackColor = true;
            this.rbTangentAll.CheckedChanged += new System.EventHandler(this.rbTangentView_CheckedChanged);
            // 
            // chkShowTangentPoly
            // 
            this.chkShowTangentPoly.AutoSize = true;
            this.chkShowTangentPoly.Location = new System.Drawing.Point(15, 61);
            this.chkShowTangentPoly.Name = "chkShowTangentPoly";
            this.chkShowTangentPoly.Size = new System.Drawing.Size(130, 17);
            this.chkShowTangentPoly.TabIndex = 4;
            this.chkShowTangentPoly.Text = "show tangent polygon";
            this.chkShowTangentPoly.UseVisualStyleBackColor = true;
            this.chkShowTangentPoly.CheckedChanged += new System.EventHandler(this.chkShowTangentPoly_CheckedChanged);
            // 
            // trcTangentPolyArg
            // 
            this.trcTangentPolyArg.AutoSize = false;
            this.trcTangentPolyArg.Location = new System.Drawing.Point(9, 36);
            this.trcTangentPolyArg.Maximum = 100;
            this.trcTangentPolyArg.Name = "trcTangentPolyArg";
            this.trcTangentPolyArg.Size = new System.Drawing.Size(148, 33);
            this.trcTangentPolyArg.TabIndex = 3;
            this.trcTangentPolyArg.TickStyle = System.Windows.Forms.TickStyle.None;
            this.trcTangentPolyArg.Value = 1;
            this.trcTangentPolyArg.Scroll += new System.EventHandler(this.trcTangentPolyArg_Scroll);
            // 
            // oxyPlotView1
            // 
            this.oxyPlotView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.oxyPlotView1.Location = new System.Drawing.Point(0, 0);
            this.oxyPlotView1.Name = "oxyPlotView1";
            this.oxyPlotView1.Size = new System.Drawing.Size(536, 497);
            this.oxyPlotView1.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(721, 497);
            this.Controls.Add(this.splitContainer1);
            this.DoubleBuffered = true;
            this.Name = "MainForm";
            this.Text = "Form1";
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel1.PerformLayout();
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trcTangentPolyArg)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private OxyPlotView oxyPlotView1;
        private System.Windows.Forms.CheckBox chkShowControlPoly;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.TrackBar trcTangentPolyArg;
        private System.Windows.Forms.CheckBox chkShowTangentPoly;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rbTangentsOnly;
        private System.Windows.Forms.RadioButton rbTangentAll;
        private System.Windows.Forms.RadioButton rbTangentintersectsOnly;
    }
}

